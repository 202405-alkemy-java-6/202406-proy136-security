package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ejemplo")
public class EjemploController {

	@Value("${rol.name}")
	private String rol;
	
	@GetMapping("/holamundo/{nombre}")
	public ResponseEntity<String> holaMundo(@PathVariable("nombre") String nombre) {

		return new ResponseEntity<>("Hola "+nombre+" desde Springboot. Rol cargado es: "+rol, HttpStatus.OK);
		
	}


	@GetMapping("/holamundo")
	public ResponseEntity<String> holaMundo() {
		
		return new ResponseEntity<>("Hola 2 desde Springboot. Rol cargado es: "+rol, HttpStatus.OK);
		
	}

}
